<?php

require_once('animal.php');
require_once('frog.php');
require_once('ape.php');

$sheep = new Animal("shaun");

echo "Nama Animal : $sheep->name <br>";
echo "Legs : $sheep->legs <br>";
echo "Cold Blooded : $sheep->cold_blooded <br><br>";

$kodok = new Frog ("buduk");

echo "Nama Animal : $kodok->name <br>";
echo "Legs : $kodok->legs <br>";
echo "Cold Blooded : $kodok->cold_blooded <br>";
echo "Hop Hop <br><br>" . $kodok->jump();

$sungokong = new Ape("kera sakti");
echo "Nama Animal : $sungokong->name <br>";
echo "Legs : $sungokong->legs <br>";
echo "Cold Blooded : $sungokong->cold_blooded <br>";
echo "Auooo <br><br>" . $sungokong->yell();

?>



